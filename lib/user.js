var mongoose = require('mongoose');
var mongooseAuth = require('mongoose-auth');

// setup everyauth/mongooseAuth module, User model
var Schema = mongoose.Schema;
var ObjectId = mongoose.SchemaTypes.ObjectId;

var UserSchema = new Schema({});
var User;

UserSchema.plugin(mongooseAuth, {
  everymodule: {
      everyauth: {
        User: function () {
          return User;
        }
      }
  },
  password: {
    loginWith: 'email',
    extraParams: {
      territory: String,
      name: {
        first: String,
        last: String
      }
    },
    everyauth: {
      getLoginPath: '/login',
      postLoginPath: '/',
      loginView: 'index.jade',
      loginLocals: {title : 'Welcome', screenId : 'home'},
      getRegisterPath: '/register',
      postRegisterPath: '/register',
      registerView: 'register.jade',
      registerLocals: {title : 'Register', screenId : 'register'},
      loginSuccessRedirect: '/account',
      registerSuccessRedirect: '/account',
      validateRegistration: function (newUserAttributes, baseErrors) {
        if (!newUserAttributes.name.first) baseErrors.push('Please enter your first name.');
        if (!newUserAttributes.name.last) baseErrors.push('Please enter your last name.');
        return baseErrors;
      }
    }
  }
});

module.exports = function(){

  User = mongoose.model('User', UserSchema);
  return User;

};