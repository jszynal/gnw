var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var Policy;

var PolicySchema = new Schema({
  _creator      : { type: ObjectId, ref: 'User' },
  customer      : { type: String },
  type          : { type: String },
  description   : { type: String },
  createdAt     : { type: Date, default: new Date() },
  updatedAt     : { type: Date, default: new Date() }
});

module.exports = function(){

  Policy = mongoose.model('Policy', PolicySchema);
  return Policy;

};