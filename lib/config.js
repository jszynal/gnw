var express = require('express');
var mongooseAuth = require('mongoose-auth');
var assetManager = require('connect-assetmanager');

module.exports = function(app, approot){
  // Configuration
// common config
  app.configure(function(){
    app.set('views', approot + '/views');
    app.set('view engine', 'jade');
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser());
    app.use(express.session({ secret: 'your secret here' }));
    app.use(mongooseAuth.middleware());
    app.expose({ENV : app.settings.env}, 'GNWApp');
  });
// dev config, keys off of NODE_ENV environment variable
  app.configure('development', function(){
    app.use(assetManager(require('./assetManagerGroups').groups(false, true, app)));
    app.use(express.static(approot + '/public'));
    app.use(express.csrf());
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
  });
// production config, keys off of NODE_ENV environment variable
  app.configure('production', function(){
    app.use(assetManager(require('./assetManagerGroups').groups(false, true, app)));
    app.use(express.static(approot + '/public'));
    app.use(express.csrf());
    app.use(express.errorHandler());
  });
};