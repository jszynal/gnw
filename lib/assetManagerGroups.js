exports.groups = function(isStale,isDebug,app){

  return {

    'common-css' : {
      'route': /\/stylesheets\/common-styles.css/,
      'path': './public/stylesheets/',
      'dataType': 'css',
      'files': ['bootstrap.min.css','style.css'],
      'stale' : isStale,
      'debug' : isDebug
    },

    'common-scripts': {
      'route': /\/javascripts\/common-scripts.js/,
      'path': './public/javascripts/lib/',
      'dataType': 'javascript',
      'files': [
        'jquery-1.7.1.min.js',
        'underscore.min.js',
        'backbone.min.js',
        'bootstrap-modal.js',
        'bootstrap-alerts.js',
        'bootstrap-twipsy.js',
        'bootstrap-popover.js',        
        'bootstrap-dropdown.js',
        'bootstrap-tabs.js',
        'bootstrap-buttons.js',
        'jquery.tmpl.min.js',
        '../app.js'
      ]
    }
  };

};