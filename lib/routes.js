module.exports = function(app){
  // Here we grab the "_" underscore object from app
  // Check http://documentcloud.github.com/underscore for Underscore.js documentation
  var _ = app._;

  // Route: root url
  // We use underscore (map/pluck) & express-expose to massage then make available these policy
  // documents from MongoDB directly in client HTML via javascript object literals. We want to 
  // work with policy documents via client-side javascript (Backbone.js) and this is how we do it.
  // View source on homepage and you'll see our policies represented as javascript object literals
  app.get('/', function(req,res){
    app.Policy.find({},['customer','type','description','createdAt','updatedAt'], function(err,docs){
      var policies = _.map(_.pluck(docs,'_doc'), function(doc){
        doc._id = doc._id.toString();
        return doc;
      });
      res.expose({data: policies}, 'GNWPOLICIES', 'exposedPolicies');
      res.render('index', {screenId : 'home', title : 'Welcome'});  
    });
  });

  // Route: logout
  app.get('/logout', function(req,res){
    req.logout();
    res.redirect('/');
  });

  // Route: account, we use isLoggedIn custom middleware to check for authorized user
  app.get('/account', isLoggedIn, function(req,res){
    //console.dir(req.user);
    res.render('account', {screenId : 'account', title : 'Account'});	
  });

  // Route: create policy
  app.post('/api/policy', function(req,res){
    var p = new app.Policy({customer: req.body.customer, type: req.body.type, description: req.body.description});
    p.save(function(err,policy){
      if(err){res.send(400);}
      else{
        res.send(policy);
      }
    });
  });

  /* Socket.io Listeners
  app.io.sockets.on('connection', function (socket) {
    socket.emit('news', { hello: 'world' });
    socket.on('my other event', function (data) {
      console.log(data);
    });
  });
  */
};


// Custom middleware function for protected pages
function isLoggedIn(req,res,next){
  if (req.loggedIn){
    next();
  } else {
    req.flash('You have been logged out');
    res.redirect('/');	
  }
}