/* 
  Visit http://documentcloud.github.com/backbone for usage details
  concerning Backbone.js, an awesome client-side MVC javascript framework
  used here.
*/   


/************************/
/*  Global App Object   */
/************************/
var GNWApp = GNWApp || {};
GNWApp.models = GNWApp.models || {};
GNWApp.collections = GNWApp.collections || {};
GNWApp.views = GNWApp.views || {};
GNWApp.util = GNWApp.util || {};


// We use CSRF protection from Express. A unique CSRF token
// can be found in META tag on every page. On every Backbone.js
// client-side model sync (create/update/delete), we include 
// this CSRF token with sent data
Backbone.Model.prototype.toJSON = function(){
  return _(_.clone(this.attributes)).extend({
    '_csrf' : $('meta[name="csrf-token"]').attr('content')
  });
};


/************************/
/*  Models/Collections  */
/************************/
GNWApp.models.User = Backbone.Model.extend({

  // urlRoot : this is like the base REST url for Backbone.js sync()
  //           calling .save() on new model will POST here, existing
  //           model will PUT here.
  urlRoot: '/api/user',
  // idAttribute : changed from 'id' Backbone default to '_id' becuase that's what
  //               MongoDB documents use as their ID making Backbone.Model -> MongoDB Doc 
  //               cleaner.
  idAttribute: '_id'

});

GNWApp.models.Policy = Backbone.Model.extend({
  urlRoot: '/api/policy',
  idAttribute: '_id'
});

GNWApp.collections.Policies = Backbone.Collection.extend({
  model: GNWApp.models.Policy
});



/************************/
/*  Views               */
/************************/
GNWApp.views.HomeView = Backbone.View.extend({
  el: 'body',

  events: {
    'click a#save-btn' : 'createPolicyHandler',
    'click a#cancel-btn' : 'cancelPolicy'
  },

  initialize: function(){
    this._policyContainer = self.$('#policy-container');
    this.renderPolicies();
  },

  createPolicyHandler: function(e){
    var self = this;
    var p = new GNWApp.models.Policy({customer: self.$('#customer').val(), type: self.$('#policy-type').val(), description: self.$('#policy-description').val()});
    p.save({},{
      success: function(model,res){
        console.dir(res);
        self.resetPolicyForm();
        self.$('#policy-form-modal').modal('hide');
      },
      error: function(){
        console.log('there was an error');
      }
    });
    e.preventDefault();
  },

  cancelPolicy: function(e){
    this.resetPolicyForm();
    this.$('#policy-form-modal').modal('hide');
    e.preventDefault();
  },

  resetPolicyForm: function(){
    this.$('#customer').val('');
    this.$('#policy-description').val('');
  },

  renderPolicies: function(){
    var self = this;
    this.collection.each(function(policy){
      var view = new GNWApp.views.Policy({model: policy});
      view.render().appendTo(self._policyContainer);
    }); 
  }

  
});

GNWApp.views.Policy = Backbone.View.extend({
  tagName: 'div',
  className: 'policy-item',

  initialize: function(){
    this._viewTmpl = $('#mainPolicyTmpl');
  },

  render: function(){
    var self = this;
    $(self.el).html(self._viewTmpl.tmpl(self.model.attributes));
    return $(self.el);
  }
});

GNWApp.views.Account = Backbone.View.extend({
  el: 'div#account-screen',

  events: {
    
  },

  initialize: function(){
    
  },

});

/************************/
/*  Util                */
/************************/
GNWApp.config = {

  initSockets: function(){
    var socket = io.connect('http://localhost');
    socket.on('news', function (data) {
      console.log(data);
      socket.emit('my other event', { my: 'data' });
    });
  }

};


/************************/
/*  App Entry Point     */
/************************/
GNWApp.run = function(){
  var MainView, AccountView, screedId;
  screenId = $('body').attr('id');     // ID attrib of body tag used to identify which screen
  
  switch(screenId){
    case 'home':
      window.HomeView = new GNWApp.views.HomeView({collection: new GNWApp.collections.Policies(GNWPOLICIES.data)});
      break;
    case 'account':
      AccountView = new GNWApp.views.Account();
      //console.log('account page');
      break;
    default:
      //console.log('other page');
  }

};


/************************/
/*  Fire on DOM ready   */
/************************/
$(function(){

  GNWApp.run();

});	
