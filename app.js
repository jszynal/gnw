// Module dependencies
var express = require('express');
var mongooseAuth = require('mongoose-auth');
var app = module.exports = express.createServer();
var expose = require('express-expose');
app._ = require('underscore');
//app.io = require('socket.io').listen(app);

// Connect to MongoLab and init User model
require('./lib/db').connect();
var User = require('./lib/user')();
app.Policy = require('./lib/policy')();

// Config App Environments
require('./lib/config')(app,__dirname);

// Setup controller routes
require('./lib/routes')(app);

// Import static helpers
app.helpers(require('./lib/staticHelpers')(app));
// Import my own dynamic helpers (each method in object has access to req/res)
app.dynamicHelpers(require('./lib/dynamicHelpers'));
// Give views access to MongooseAuth helpers
mongooseAuth.helpExpress(app);

//Listen on port assigned by environment (ie. Heroku, production) or default (development)
app.listen(process.env.PORT || 3000);
console.log("Express server listening on port %d in %s mode. Node Version: %s", app.address().port, app.settings.env, process.version);
